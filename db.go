package toollib

import (
	"bytes"
	"database/sql"
	"runtime/debug"
	//"flag"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

type StoreConfig struct {
	MysqlHost   string // Fully-qualified host descriptor for connection to MySQL.
	MysqlUser   string
	MysqlPasswd string
	MysqlDbname string
}

type Store struct {
	config          StoreConfig
	db              *sql.DB
	stmtAddNewItem  *sql.Stmt
	stmtGetItem     *sql.Stmt
	stmtSearchItems *sql.Stmt
	stmtGetUserId   *sql.Stmt
}

// Panics if db connection fails or statement prep fails.
func NewStore(c StoreConfig) *Store {
	store := new(Store)
	store.config = c

	db_connection_string := bytes.NewBufferString("")
	fmt.Fprintf(db_connection_string,
		"%s:%s@%s/%s",
		c.MysqlUser, c.MysqlPasswd, c.MysqlHost, c.MysqlDbname)
	var sqlerr error
	store.db, sqlerr = sql.Open("mysql", db_connection_string.String())
	if sqlerr != nil {
		log.Fatal("Cannot connect to database. Error: ", sqlerr.Error())
	}

	store.stmtAddNewItem = store.mustPrepare("INSERT INTO items(shortdesc, description, owner, posted) " +
		"VALUES(?, ?, ?, ?)")
	store.stmtGetItem = store.mustPrepare("SELECT shortdesc, description FROM items WHERE item_id = ?")
	store.stmtSearchItems = store.mustPrepare("SELECT *, MATCH(shortdesc, description) " +
		"AGAINST(? IN BOOLEAN MODE) AS score FROM items WHERE MATCH(shortdesc, description) " +
		"AGAINST (? IN BOOLEAN MODE);")
	store.stmtGetUserId = store.mustPrepare("SELECT user_id FROM user_credentials " +
		"WHERE auth_provider = ? AND auth_provider_id = ?;")

	return store
}

func (store *Store) Close() {
	store.stmtAddNewItem.Close()
	store.stmtGetItem.Close()
	store.stmtSearchItems.Close()
	store.stmtGetUserId.Close()
	store.db.Close()
}

// TODO: I'd prefer to encapsulate away direct access to results, and just return the data.
func (store *Store) AddNewItem(item *Item) (sql.Result, error) {
	return store.stmtAddNewItem.Exec(item.ShortDesc, item.Description, item.Owner, item.Posted)
}

func (store *Store) GetItem(id string, item *Item) error {
	return store.stmtGetItem.QueryRow(id).Scan(&item.ShortDesc, &item.Description)
}

// TODO: I'd prefer to encapsulate away direct access to results, and just return the data.
func (store *Store) SearchItems(terms string) (*sql.Rows, error) {
	return store.stmtSearchItems.Query(terms, terms)
}

// Returns (user_id, error).
// If the user does not exist in the database, we DO NOT add it and return -1.
func (store *Store) GetUserId(provider string, provider_id string) (int, error) {
	log.Printf("Provider, id: %s, %s\n", provider, provider_id)
	id := -1
	rows, err := store.stmtGetUserId.Query(provider, provider_id)
	defer rows.Close()
	if err != nil {
		return -2, errors.New("Failed to retrieve user_id.")
	}

	items_returned := 0
	for rows.Next() {
		if items_returned != 0 {
			log.Fatal("stmtGetUserId returned more than one row!")
		}
		err = rows.Scan(&id)
		if err != nil {
			return -2, errors.New("Failed to scan user_id.")
		}
		items_returned++
	}

	return id, nil
}

func (store *Store) mustPrepare(query string) *sql.Stmt {
	stmt, err := store.db.Prepare(query)
	if err != nil {
		debug.PrintStack()
		log.Fatalf("Failed to prepare statement: '%s' Err: %s", query, err.Error())
	}
	return stmt
}
