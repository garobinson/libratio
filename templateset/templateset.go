package templateset

import (
	"html/template"
	"log"
)

type TemplateSet struct {
	dir       string
	rootdir   string
	root      *template.Template
	templates map[string]*template.Template
}

func NewTemplateSet(dir string, rootdir string) TemplateSet {
	set := TemplateSet{dir: dir, rootdir: rootdir}
	set.root = template.Must(
		template.ParseGlob(rootdir))
	set.templates = make(map[string]*template.Template)
	return set
}

func (set *TemplateSet) Get(name string) *template.Template {
	if set.templates[name] != nil {
		return set.templates[name]
	}
	if set.root == nil {
		log.Fatal("Template root not set!")
	}
	set.templates[name], _ = set.root.Clone()
	template.Must(set.templates[name].ParseFiles(set.dir + name + ".html"))
	return set.templates[name]
}
