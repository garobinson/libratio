package main

import (
	"flag"
	"github.com/fiatflux/toollib"
	"github.com/fiatflux/toollib/auth"
	"github.com/fiatflux/toollib/templateset"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/stretchr/gomniauth"
	gomniauth_github "github.com/stretchr/gomniauth/providers/github"
	gomniauth_google "github.com/stretchr/gomniauth/providers/google"
	"github.com/stretchr/signature"
	"log"
	"net/http"
	"time"
)

var (
	mysql_user   = flag.String("mysql_user", "toollib", "Username for connection to MySQL db.")
	mysql_passwd = flag.String("mysql_passwd", "8yghjYTGF", "Password for connection to MySQL db.")
	mysql_dbname = flag.String("mysql_dbname", "toollib", "Name of MySQL database.")
	mysql_host   = flag.String("mysql_host",
		"tcp(127.0.0.1:3306)",
		"Fully-qualified host descriptor for connection to MySQL.")
	http_listen_socket = flag.String(
		"http-listen-socket",
		":8080",
		"HTTP socket on which to listen.")

	template_dir = *flag.String("template_dir", "resources/templates/",
		"Template directory relative to cwd.")
	template_root = *flag.String("template_root", "resources/templates/root/*",
		"Template globals directory relative to cwd.")

	oauth_google_id       = flag.String("oauth_google_id", "", "App id for Google OAuth.")
	oauth_google_secret   = flag.String("oauth_google_secret", "", "Secret for Google OAuth.")
	oauth_google_callback = flag.String("oauth_google_callback",
		"http://localhost:8080/auth/google/callback", "Callback URL for Google OAuth.")
	oauth_github_id       = flag.String("oauth_github_id", "", "App id for Github OAuth.")
	oauth_github_secret   = flag.String("oauth_github_secret", "", "Secret for Github OAuth.")
	oauth_github_callback = flag.String("oauth_github_callback",
		"http://localhost:8080/auth/google/callback", "Callback URL for Github OAuth.")

	db *toollib.Store

	//httpRequestLogFile *os.File
)

// Thanks to https://github.com/jmckaskill/krb-httpd for loggedResponse.
type loggedResponse struct {
	w       http.ResponseWriter
	r       *http.Request
	url     string
	t       time.Time
	replied bool
}

func (w *loggedResponse) Flush() {
	if wf, ok := w.w.(http.Flusher); ok {
		wf.Flush()
	}
}
func (w *loggedResponse) Header() http.Header { return w.w.Header() }
func (w *loggedResponse) Write(d []byte) (int, error) {
	if !w.replied {
		w.WriteHeader(http.StatusOK)
	}
	return w.w.Write(d)
}
func (w *loggedResponse) WriteHeader(status int) {
	if !w.replied {
		w.replied = true
		log.Printf("%s %s \"%s %s %s\" %s %d", w.t.Format(time.RFC3339Nano), w.r.RemoteAddr, w.r.Method, w.url, w.r.Proto, w.r.Referer(), status)
		w.w.WriteHeader(status)
	} else {
		log.Print("Multiple calls to WriteHeader.")
	}
}

func logHttp(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w2 http.ResponseWriter, r *http.Request) {
		w := &loggedResponse{w2, r, r.URL.String(), time.Now(), false}
		fn(w, r)
	}
}

func main() {
	flag.Parse()

	db = toollib.NewStore(toollib.StoreConfig{MysqlHost: *mysql_host, MysqlUser: *mysql_user,
		MysqlPasswd: *mysql_passwd, MysqlDbname: *mysql_dbname})
	defer db.Close()

	templates := templateset.NewTemplateSet(
		template_dir, template_root)
	gomniauth.SetSecurityKey(signature.RandomKey(64))
	providers := gomniauth.WithProviders(
		gomniauth_google.New(*oauth_google_id,
			*oauth_google_secret,
			*oauth_google_callback),
		gomniauth_github.New(*oauth_github_id,
			*oauth_github_secret,
			*oauth_github_callback))
	session_options := &auth.SessionOptions{
		Domain:   "",
		Path:     "/",
		MaxAge:   0,
		HttpOnly: false,
		Secure:   false,
	}
	config := auth.AuthenticatorConfig{"librat", "user", session_options}
	store := sessions.NewCookieStore([]byte(signature.RandomKey(64)))
	authenticator := auth.NewGomniAuthenticator(
		config,
		db,
		store,
		providers,
		templates)
	toollib.Authenticator = authenticator

	toollib.SetGlobalStore(db)

	toollib.Templates = templates

	// Serve static css.
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("resources/static/css"))))
	// Serve authenticator services.
	http.Handle(toollib.AuthBaseURI, authenticator.GetRouter())

	mainrouter := mux.NewRouter()
	mainrouter.StrictSlash(true)
	mainrouter.HandleFunc(toollib.ShowItemURI, logHttp(authenticator.ProcessUserCookie(toollib.HandleShow)))
	mainrouter.HandleFunc(toollib.SearchURI, logHttp(authenticator.ProcessUserCookie(toollib.HandleSearch)))
	mainrouter.HandleFunc(toollib.NewItemURI, logHttp(authenticator.RequireLogin(toollib.HandleNewItem)))
	mainrouter.HandleFunc(toollib.NewItemFormURI, logHttp(authenticator.RequireLogin(toollib.HandleNewItemForm)))
	mainrouter.HandleFunc(toollib.SearchBoxURI, logHttp(authenticator.ProcessUserCookie(toollib.HandleIndex)))
	http.Handle("/", mainrouter)

	err := http.ListenAndServe(*http_listen_socket, nil)
	if err != nil {
		log.Printf("Error serving http: %s\n", err.Error())
	}
}
