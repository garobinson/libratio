package toollib

import (
	"bytes"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"testing"
)

const (
	mysql_host = "tcp(127.0.0.1:3306)"
	mysql_user = "toollib_test"
	mysql_pw   = "toollib_test_pw"
	mysql_db   = "toollib_test_db"
)

var (
	testdb *sql.DB
)

func Setup(t *testing.T) {
	db_connection_string := bytes.NewBufferString("")
	fmt.Fprintf(db_connection_string,
		"%s:%s@%s/%s",
		mysql_user, mysql_pw, mysql_host, mysql_db)
	var sqlerr error
	testdb, sqlerr = sql.Open("mysql", db_connection_string.String())
	if sqlerr != nil {
		t.Fatalf("Cannot connect to database. Error: %s.\n", sqlerr.Error())
	}

	// TODO: This should use the real script or copy the db schema or something.
	dropUsersScript := "DROP TABLE IF EXISTS users;"
	dropUserCredsScript := "DROP TABLE IF EXISTS user_credentials;"
	setupUsersScript := `
CREATE TABLE users (
    user_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(254) NOT NULL,
    custom_email VARCHAR(254) NOT NULL,
    name VARCHAR(100) NOT NULL,
    custom_name VARCHAR(100) NOT NULL,
    nickname VARCHAR(100) NOT NULL,
    custom_nickname VARCHAR(100) NOT NULL,
    joined DATETIME NOT NULL,
    last_seen DATETIME NOT NULL,
    isnew BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
	setupUserCredsScript := `
CREATE TABLE user_credentials (
    auth_provider VARCHAR(10) NOT NULL,
    auth_provider_id VARCHAR(15) NOT NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY(auth_provider, auth_provider_id),
    FOREIGN KEY(user_id) REFERENCES users(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;`

	if _, err := testdb.Exec(dropUserCredsScript); err != nil {
		t.Fatalf("Failed to execute setup script. Error: %s\n", err.Error())
	}
	if _, err := testdb.Exec(dropUsersScript); err != nil {
		t.Fatalf("Failed to execute setup script. Error: %s\n", err.Error())
	}
	if _, err := testdb.Exec(setupUsersScript); err != nil {
		t.Fatalf("Failed to execute setup script. Error: %s\n", err.Error())
	}
	if _, err := testdb.Exec(setupUserCredsScript); err != nil {
		t.Fatalf("Failed to execute setup script. Error: %s\n", err.Error())
	}
}

func TearDown(t *testing.T) {
	cleanupScript := "DROP TABLE IF EXISTS user_credentials;"
	if _, err := testdb.Exec(cleanupScript); err != nil {
		t.Fatalf("Failed to tear down.")
	}
}

func TestGetUserId(t *testing.T) {
	Setup(t)
	defer TearDown(t)

	storeconfig := StoreConfig{
		MysqlHost:   mysql_host,
		MysqlUser:   mysql_user,
		MysqlPasswd: mysql_pw,
		MysqlDbname: mysql_db,
	}
	//t.Fatalf("store config: %s", storeconfig)
	store := NewStore(storeconfig)
	defer store.Close()

	provider := "someProv"
	provider_id := "someProviderId"

	user_id, err := store.GetUserId(provider, provider_id)
	if user_id != -1 {
		t.Fatal("Expected user_id = -1 but got %d.", user_id)
	}
	if err != nil {
		t.Fatal("Error encountered: ", err.Error())
	}

	// Try adding a foreign-key constraint violation.
	_, err = testdb.Exec(`INSERT INTO user_credentials VALUES ('someProv', 'someProviderId', 42);`)
	if err == nil {
		t.Fatal("Expected error condition but got nil.")
	}

	// Add the user.
	_, err = testdb.Exec(`INSERT INTO users(user_id) VALUES(42);`)
	if err != nil {
		t.Fatal("Error encountered: ", err.Error())
	}
	user_id, err = store.GetUserId(provider, provider_id)
	if user_id != -1 {
		t.Fatalf("Expected user_id = -1 but got %d.", user_id)
	}
	if err != nil {
		t.Fatal("Error encountered: ", err.Error())
	}

	// Now check for existing credentials.
	_, err = testdb.Exec(`INSERT INTO user_credentials VALUES ('someProv', 'someProviderId', 42);`)
	if err != nil {
		t.Fatal("Error encountered: ", err.Error())
	}
	user_id, err = store.GetUserId(provider, provider_id)
	if user_id != 42 {
		t.Fatalf("Expected user_id = 42 but got %d.", user_id)
	}
	if err != nil {
		t.Fatal("Error encountered: ", err.Error())
	}
}
