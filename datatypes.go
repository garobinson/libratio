package toollib

import (
	"time"
)

type User struct {
	// schema tags are for use by gorilla/schema.
	// Schema tags omitted iff we don't want the user editing the respective field.
	UserId         string    `schema:"-"`
	Email          string    `schema:"-"`
	Name           string    `schema:"-"`
	Nickname       string    `schema:"-"`
	AuthProvider   string    `schema:"-"`
	AuthProviderId string    `schema:"-"`
	Joined         time.Time `schema:"-"`
	LastSeen       time.Time `schema:"-"`
	CustomName     string    `schema:"CustomName"`
	CustomNickName string    `schema:"CustomNickName"`
	CustomEmail    string    `schema:"CustomEmail"`
	Phone          string    `schema:"Phone"`
}

type Item struct {
	// schema tags are for use by gorilla/schema.
	// Schema tags omitted iff we don't want the user editing the respective field.
	ItemId      int       `schema:"-"`
	ShortDesc   string    `schema:"shortdesc"`
	Description string    `schema:"desc"`
	Owner       int       `schema:"-"`
	Posted      time.Time `schema:"-"`
}
