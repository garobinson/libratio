CREATE DATABASE toollib CHARACTER SET utf8 COLLATE utf8_bin;
USE toollib;

CREATE USER 'toollib'@'localhost' IDENTIFIED BY '8yghjYTGF';
GRANT ALL PRIVILEGES ON `toollib` . * TO 'toollib'@'localhost';

CREATE TABLE users (
    user_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(254) NOT NULL,
    custom_email VARCHAR(254) NOT NULL,
    name VARCHAR(100) NOT NULL,
    custom_name VARCHAR(100) NOT NULL,
    nickname VARCHAR(100) NOT NULL,
    custom_nickname VARCHAR(100) NOT NULL,
    joined DATETIME NOT NULL,
    last_seen DATETIME NOT NULL,
    isnew BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user_credentials (
    auth_provider VARCHAR(10) NOT NULL,
    auth_provider_id VARCHAR(25) NOT NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY(auth_provider, auth_provider_id),
    FOREIGN KEY(user_id) REFERENCES users(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE items (
    item_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    shortdesc VARCHAR(140) NOT NULL,
    description VARCHAR(4096) NOT NULL,
    owner INT(11) NOT NULL,
    posted DATETIME NOT NULL,
    FOREIGN KEY (owner) REFERENCES users(user_id),
    FULLTEXT (shortdesc, description)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE error_messages(
    error_message VARCHAR(65),
    PRIMARY KEY(error_message)
);

INSERT INTO error_messages VALUES ('INSERT: non-existing user_id');

DELIMITER |
CREATE TRIGGER insert_items
  BEFORE INSERT
  ON items
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM users WHERE user_id=new.owner)=0
    THEN
      INSERT error_messages VALUES ('INSERT: non-existing user_id');
    END IF;
  END; |
DELIMITER ;
