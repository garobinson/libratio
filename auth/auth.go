package auth

import (
	"encoding/gob"
	"github.com/fiatflux/toollib/templateset"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/stretchr/gomniauth"
	gomniauth_common "github.com/stretchr/gomniauth/common"
	//gomniauth_github "github.com/stretchr/gomniauth/providers/github"
	//gomniauth_google "github.com/stretchr/gomniauth/providers/google"
	"github.com/stretchr/objx"
	//"github.com/stretchr/signature"
	//"html/template"
	"log"
	"net/http"
	"time"
)

type SessionOptions sessions.Options

type UserCookie struct {
	LoggedIn  bool
	ID        int
	Email     string
	LoginTime time.Time
	LastSeen  time.Time // automatically updated by SetUserCookie
}

var (
	defaultUserCookie = UserCookie{
		LoggedIn: false,
		ID:       -1,
	}
)

type Authenticator interface {
	GetUserCookie(r *http.Request) (*UserCookie, error)
	SetUserCookie(w http.ResponseWriter, r *http.Request, usercookie *UserCookie) error
	RequireLogin(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc
	ProcessUserCookie(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc

	// Deals with login/logout actions.
	GetRouter() *mux.Router
}

type AuthenticatorConfig struct {
	// Session cookie name.
	SessionName string
	// Session cookie variable name.
	SessionVar string
	// Options for session cookie.
	SessionOptions *SessionOptions
}

type StoreWithCleanup interface {
	// Implement gorilla/sessions.Store
	Get(r *http.Request, name string) (*sessions.Session, error)
	New(r *http.Request, name string) (*sessions.Session, error)
	Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error

	// Add methods that should be in gorilla/sessions.Store interface.
	Delete(r *http.Request, w http.ResponseWriter, s *sessions.Session)
	Close()
}

type UserDB interface {
	// Get a user id from datastore, and create if it doesn't exist already.
	GetUserId(provider string, provider_id string) (int, error)
}

type FakeUserDB struct{}

func (udb *FakeUserDB) GetUserId(provider string, provider_id string) (int, error) {
	return -1, nil
}

type GomniAuthenticator struct {
	config          AuthenticatorConfig
	userdb          UserDB
	store           sessions.Store
	oauth_providers *gomniauth.ProviderList
	templates       templateset.TemplateSet // Templates for login page, etc.
}

func NewGomniAuthenticator(
	config AuthenticatorConfig,
	userdb UserDB,
	store sessions.Store,
	providers *gomniauth.ProviderList,
	templateset templateset.TemplateSet,
) *GomniAuthenticator {
	// TODO: where's a better place to register this?
	gob.Register(&UserCookie{})

	a := GomniAuthenticator{}
	a.config = config
	a.userdb = userdb
	a.store = store
	a.oauth_providers = providers
	a.templates = templateset
	return &a
}

// Prepare a request router that handles authentication calls.
func (a *GomniAuthenticator) GetRouter() *mux.Router {
	router := mux.NewRouter()
	// TODO: bind these to just "/{oauthprovider}/authorize", etc
	router.HandleFunc("/auth/{oauthprovider}/authorize", a.handleAuthorize)
	router.HandleFunc("/auth/{oauthprovider}/callback", a.handleOAuthCallback)
	router.HandleFunc("/auth/login", a.handleLogin)
	router.HandleFunc("/auth/logout", a.handleLogout)
	router.HandleFunc("/auth/user", a.handleUserData)
	return router
}

// TODO: signature currently includes error, but I'm not actually sure we want to return errors.
func (a *GomniAuthenticator) GetUserCookie(r *http.Request) (*UserCookie, error) {
	session, err := a.store.Get(r, a.config.SessionName)
	if err != nil {
		log.Printf("Failed to get session cookie %s. Error: %s\n", a.config.SessionName, err.Error())
		return &defaultUserCookie, nil
	}

	var usercookie *UserCookie
	if session.IsNew {
		session.Options.Domain = a.config.SessionOptions.Domain
		session.Options.Path = a.config.SessionOptions.Path
		session.Options.MaxAge = a.config.SessionOptions.MaxAge
		session.Options.HttpOnly = a.config.SessionOptions.HttpOnly
		session.Options.Secure = a.config.SessionOptions.Secure
		usercookie = &defaultUserCookie
	} else {
		var ok bool
		usercookie, ok = session.Values[a.config.SessionVar].(*UserCookie)
		if !ok {
			// TODO: return error?
			log.Printf("Invalid conversion of cookie data. URL: %s\n", r.URL)
			usercookie = &defaultUserCookie
		}
		//log.Printf("session retrieved: %s\n", usercookie)
	}

	return usercookie, nil
}

func (a *GomniAuthenticator) SetUserCookie(w http.ResponseWriter, r *http.Request, usercookie *UserCookie) error {
	// Throw away error since we log it in GetUserCookie()
	// TODO: should we do something else here?
	session, _ := a.store.Get(r, a.config.SessionName)
	usercookie.LastSeen = time.Now()
	session.Values[a.config.SessionVar] = usercookie
	//log.Printf("session saved: %s\n", usercookie)
	return session.Save(r, w)
}

// Process user cookie and add it to context.
func (a *GomniAuthenticator) ProcessUserCookie(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		usercookie, err := a.GetUserCookie(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		// Update.
		err = a.SetUserCookie(w, r, usercookie)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		context.Set(r, a.config.SessionVar, usercookie)
		fn(w, r)
	}
}

// Process user cookie, add it to context, and forward to login screen if not authenticated.
func (a *GomniAuthenticator) RequireLogin(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		usercookie, err := a.GetUserCookie(r)
		if err != nil {
			return
		}
		a.SetUserCookie(w, r, usercookie)
		if usercookie.LoggedIn == true {
			fn(w, r)
		} else {
			// TODO: use a flash for the redirection.
			// TODO: don't hardcode URI.
			http.Redirect(w, r, "/auth/login?redirect="+r.URL.String(), http.StatusSeeOther)
		}
	}
}

// Displays some session data. Useful for debugging.
func (a *GomniAuthenticator) handleUserData(w http.ResponseWriter, r *http.Request) {
	usercookie, err := a.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// Must store old cookie values since SetUserCookie updates it.
	var oldcookie UserCookie = *usercookie
	a.SetUserCookie(w, r, usercookie)
	err = a.templates.Get("userdata").Execute(w, map[string]interface{}{
		"UserData":  usercookie,
		"OldCookie": oldcookie,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (a *GomniAuthenticator) handleAuthorize(w http.ResponseWriter, r *http.Request) {
	usercookie, err := a.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	a.SetUserCookie(w, r, usercookie)

	// TODO: use flash for this.
	redirect := r.FormValue("redirect")
	if redirect == "" {
		redirect = "/"
	}
	if usercookie.LoggedIn {
		http.Redirect(w, r, redirect, http.StatusFound)
		return
	}

	pathvars := mux.Vars(r)
	provider, err := gomniauth.Provider(pathvars["oauthprovider"])
	if err != nil {
		log.Printf("Error: %s\n", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	state := gomniauth.NewState("redirect", redirect)
	authUrl, err := provider.GetBeginAuthURL(state, nil)

	if err != nil {
		log.Printf("Error: %s\n", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, authUrl, http.StatusFound)
}

func (a *GomniAuthenticator) handleOAuthCallback(w http.ResponseWriter, r *http.Request) {
	usercookie, err := a.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if usercookie.LoggedIn {
		state, err := gomniauth.StateFromParam(r.URL.Query()["state"][0])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if state.Get("redirect") != nil {
			http.Redirect(w, r, state.Get("redirect").MustStr(), http.StatusFound)
			return
		} else {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
	}

	pathvars := mux.Vars(r)
	provider, err := gomniauth.Provider(pathvars["oauthprovider"])
	if err != nil {
		log.Printf("error getting provider.\n")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var query objx.Map = objx.MustFromURLQuery(r.URL.RawQuery)
	creds, err := provider.CompleteAuth(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user, userErr := provider.GetUser(creds)
	if userErr != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(user.ProviderCredentials()) != 1 {
		log.Fatalf("Need exactly 1 Auth providers but got %d.",
			len(user.ProviderCredentials()))
	}

	// Take the credentials we got from OAuth and look them up in the internal database.
	// Later on, it might be nice to permit folks to associate their account to multiple
	// credentials. For now, assert exactly 1 since we don't have that logic built yet.
	var user_id int
	for provider, creds := range user.ProviderCredentials() {
		var provider_id string
		if provider == "github" {
			provider_id = user.Nickname()
		} else {
			provider_id = creds.Get(gomniauth_common.CredentialsKeyID).Str()
		}
		var userDbErr error
		user_id, userDbErr = a.userdb.GetUserId(provider, provider_id)
		if userDbErr != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	now := time.Now()
	usercookie.LoggedIn = true
	usercookie.ID = user_id
	usercookie.Email = user.Email()
	usercookie.LoginTime = now
	a.SetUserCookie(w, r, usercookie)

	state, err := gomniauth.StateFromParam(r.URL.Query()["state"][0])
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if state.Get("redirect") != nil {
		http.Redirect(w, r, state.Get("redirect").MustStr(), http.StatusFound)
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

func (a *GomniAuthenticator) handleLogin(w http.ResponseWriter, r *http.Request) {
	// TODO: use a flash.
	redirect := r.FormValue("redirect")
	if redirect == "" {
		redirect = "/"
	}
	usercookie, err := a.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = a.templates.Get("login").Execute(w, map[string]interface{}{
		"UserData": usercookie,
		"Redirect": redirect,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (a *GomniAuthenticator) handleLogout(w http.ResponseWriter, r *http.Request) {
	// TODO: use a flash.
	redirect := r.FormValue("redirect")
	if redirect == "" {
		redirect = "/"
	}
	usercookie, err := a.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	usercookie.LoggedIn = false
	a.SetUserCookie(w, r, usercookie)
	http.Redirect(w, r, redirect, http.StatusFound)
}
