package toollib

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/fiatflux/toollib/auth"
	"github.com/fiatflux/toollib/templateset"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"log"
	"net/http"
	"time"
)

var (
	Templates        templateset.TemplateSet
	MaxSearchResults = flag.Int("max_results", 20, "Maximum number of search results to show.")

	db            *Store = nil
	Authenticator auth.Authenticator
)

func SetGlobalStore(new_db *Store) {
	if db != nil {
		db.Close()
		db = nil
	}
	db = new_db
}

func HandleIndex(w http.ResponseWriter, r *http.Request) {
	userdata, err := Authenticator.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = Templates.Get("index").Execute(w, map[string]interface{}{"UserData": userdata})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func HandleNewItemForm(w http.ResponseWriter, r *http.Request) {
	userdata, err := Authenticator.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = Templates.Get("newitem").Execute(w, map[string]interface{}{"UserData": userdata})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func HandleNewItem(w http.ResponseWriter, r *http.Request) {
	var item Item

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	decoder := schema.NewDecoder()
	err = decoder.Decode(&item, r.URL.Query())
	// TODO: Switch to POST.
	//err = decoder.Decode(&item, r.PostForm())
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// TODO: Make sure this is the owner in the users table of our DB.
	item.Owner = -1
	item.Posted = time.Now()

	res, err := db.AddNewItem(&item)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Fatalf("Failed to insert %s into database: %s", item, err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Failed to get number of rows affected. %s", err)
	}
	if rowCnt != 1 {
		log.Fatalf("Affected %d rows but should have been 1; last id: %d", rowCnt, lastId)
	}

	// TODO: what's the concise way to do this?
	forwardUrl := bytes.NewBufferString(ShowItemBaseURI)
	fmt.Fprintf(forwardUrl, "/%d", lastId)
	http.Redirect(w, r, forwardUrl.String(), http.StatusFound)
}

func HandleShow(w http.ResponseWriter, r *http.Request) {
	var item Item

	vars := mux.Vars(r)
	item_id := vars["item_id"]

	err := db.GetItem(item_id, &item)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	userdata, err := Authenticator.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = Templates.Get("show").Execute(w, map[string]interface{}{
		"UserData": userdata,
		"Item":     item,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func HandleSearch(w http.ResponseWriter, r *http.Request) {
	search_query := r.URL.Query().Get("q")
	if len(search_query) == 0 {
		log.Println("URI does not appear to have a parameter q:", r.URL)
		http.Redirect(w, r, SearchBoxURI, http.StatusSeeOther)
		return
	}

	rows, err := db.SearchItems(search_query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var results []Item

	i := 0
	for rows.Next() {
		var item Item
		var posted string
		var score int
		err := rows.Scan(
			&item.ItemId,
			&item.ShortDesc,
			&item.Description,
			&item.Owner,
			&posted,
			&score)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, item)

		i++
		if i == *MaxSearchResults {
			// Returns a minimum of 1 row when there are results.
			break
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	userdata, err := Authenticator.GetUserCookie(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = Templates.Get("search").Execute(w, map[string]interface{}{
		"UserData": userdata,
		"Results":  results,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
