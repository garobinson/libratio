// URI mappings.

package toollib

var (
	// TODO: push these into templates.
	AuthBaseURI     = "/auth/"
	ShowItemBaseURI = "/show"
	ShowItemURI     = ShowItemBaseURI + "/{item_id:[0-9]+}"
	SearchURI       = "/search"
	NewItemURI      = "/post"
	NewItemFormURI  = "/newitem"
	SearchBoxURI    = "/"
)
